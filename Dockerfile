# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM golang:1.14

WORKDIR /go/src/app
COPY main.go .
ENV GOOS windows	
ENV GOARCH amd64
RUN go get -d -v ./...
RUN go install -v ./...
RUN ls -lr /
RUN echo "complete 001"

FROM ubuntu:20.04
RUN apt update && apt upgrade -y && apt install -y file
COPY --from=0 /go/bin/windows_amd64 /windows_amd64
